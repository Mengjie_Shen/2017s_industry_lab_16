package ictgradschool.industry.lab16.ex02.model;

import javax.swing.table.AbstractTableModel;
import java.util.List;


public class CourseAdapter extends AbstractTableModel implements CourseListener {
	private Course course;
	public CourseAdapter(Course course){
		this.course = course;

	}

	@Override
	public int getRowCount() {
		return this.course.size();
	}

	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
			case 0:
				return this.course.getResultAt(rowIndex)._studentID;
			case 1:
				return this.course.getResultAt(rowIndex)._studentSurname;
			case  2:
				return this.course.getResultAt(rowIndex)._studentForename;
			case 3:
				return this.course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Exam);
			case 4:
				return this.course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Test);
			case 5:
				return this.course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Assignment);
			case 6:
				return this.course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Overall);
		}
		return null;
	}

	public String getColumnName(int column) {
		switch (column){
			case 0:
				return "Student ID";
			case 1:
				return "Surname";
			case 2:
				return  "First Name";
			case 3:
				return "Exam";
			case 4:
				return "Test";
			case 5:
				return "Assignment";
			case 6:
				return "Overall";
		}
		return null;
	}
/*
	public List<StudentResult> getResults() {
		return this.results;
	}*/

	@Override
	public void courseHasChanged(Course course) {
		fireTableDataChanged();
	}
}