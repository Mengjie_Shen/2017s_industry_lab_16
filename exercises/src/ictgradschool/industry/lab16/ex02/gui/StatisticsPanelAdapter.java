package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;

public class StatisticsPanelAdapter extends AbstractTableModel implements ComboBoxModel, CourseListener{
	private Course course;

	public StatisticsPanelAdapter(Course course) {
		this.course = course;
	}
	@Override
	public void setSelectedItem(Object anItem) {

	}

	@Override
	public Object getSelectedItem() {
		return null;
	}

	@Override
	public int getSize() {
		return 0;
	}

	@Override
	public Object getElementAt(int index) {
		return null;
	}

	@Override
	public void addListDataListener(ListDataListener l) {

	}

	@Override
	public void removeListDataListener(ListDataListener l) {

	}

	@Override
	public void courseHasChanged(Course course) {
		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return 0;
	}

	@Override
	public int getColumnCount() {
		return 0;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return null;
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
	
}
