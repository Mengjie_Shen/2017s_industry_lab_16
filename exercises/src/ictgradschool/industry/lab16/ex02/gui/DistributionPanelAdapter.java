package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.*;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.HashSet;
import java.util.Set;

public class DistributionPanelAdapter extends AbstractTableModel implements CourseListener{
	private JTable tableView;
	private Set<CourseListener> listeners;
	private Course course;
	private DistributionPanel distributionPanel;

	public DistributionPanelAdapter(DistributionPanel distributionPanel, Course course) {
		this.distributionPanel = distributionPanel;
		this.course = course;
//		this.listeners = listeners;
	}

	@Override
	public void courseHasChanged(Course course) {
		this.distributionPanel.repaint();
//		DistributionPanel distributionPanel = new DistributionPanel(course);


	}

	@Override
	public int getRowCount() {
		return this.course.size();
	}

	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return null;
	}

/*	public Percentage getOverall(int rowIndex) {
		return this.courseAdapter.getResults().get(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Overall);
	}*/

//	public int getCount(int lower, )


	/**********************************************************************
	 * YOUR CODE HERE
	 */
}
